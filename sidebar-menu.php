<ul data-test-id="header_menu" class="header__main-menu_2d9">
                <li class=""><a data-test-id="home_header_mattress_link" href="/mattress">Mattress</a></li>
                <li class="dropdown_qj6"><button type="button" class="dropdown__button_nqJ" data-test-id="home_header_bed_frames_link"><span class="dropdown__button-text_3Pu">Bed
                            Frames</span><svg xmlns="http://www.w3.org/2000/svg" height="8" width="12"
                            viewBox="0 0 12 8" fill="#4C3043" class="dropdown__icon_20r">
                            <path
                                d="M10.6 0.333282L6 4.93328L1.4 0.333282L0 1.73328L6 7.73328L12 1.73328L10.6 0.333282Z"
                                fill="#C56E43"></path>
                        </svg></button>
                    <ul class="dropdown__body_3O0">
                        <li class=""><a data-test-id="home_header_platform_bed_link" href="/platform-bed">Platform
                                Bed</a></li>
                        <li class=""><a data-test-id="home_header_adjustable_bed_frame_link" href="/adjustable-bed-frame">Adjustable Frame</a></li>
                        <li class=""><a data-test-id="home_header_bed_frame_with_headboard_link" href="/bed-frame-with-headboard">Frame + Headboard</a></li>
                    </ul>
                </li>
                <li class="dropdown_qj6"><button type="button" class="dropdown__button_nqJ" data-test-id="home_header_why_latex_link"><span class="dropdown__button-text_3Pu"> Why
                            Awara?</span><svg xmlns="http://www.w3.org/2000/svg" height="8" width="12"
                            viewBox="0 0 12 8" fill="#4C3043" class="dropdown__icon_20r">
                            <path
                                d="M10.6 0.333282L6 4.93328L1.4 0.333282L0 1.73328L6 7.73328L12 1.73328L10.6 0.333282Z"
                                fill="#C56E43"></path>
                        </svg></button>
                    <ul class="dropdown__body_3O0">
                        <li class=""><a data-test-id="home_header_why_latex_link" href="https://www.awarasleep.com/p/why-latex-mattress/">Why Latex?</a></li>
                        <li class=""><a data-test-id="home_header_trial_link" href="https://www.awarasleep.com/p/trial/">365 Trial</a></li>
                        <li class=""><a data-test-id="home_header_all_natural_link" href="https://www.awarasleep.com/p/all-natural/">All Natural</a></li>
                    </ul>
                </li>
                <li class="dropdown_qj6"><button type="button" class="dropdown__button_nqJ" data-test-id="home_header_compare_link"><span
                            class="dropdown__button-text_3Pu">Compare</span><svg xmlns="http://www.w3.org/2000/svg"
                            height="8" width="12" viewBox="0 0 12 8" fill="#4C3043" class="dropdown__icon_20r">
                            <path
                                d="M10.6 0.333282L6 4.93328L1.4 0.333282L0 1.73328L6 7.73328L12 1.73328L10.6 0.333282Z"
                                fill="#C56E43"></path>
                        </svg></button>
                    <ul class="dropdown__body_3O0">
                        <li class=""><a data-test-id="home_header_compare_all_link" href="https://www.awarasleep.com/p/compare/">Compare All</a></li>
                        <li class=""><a data-test-id="home_header_compare_avocado_link" href="https://www.awarasleep.com/p/compare/avocado/">Vs Avocado</a></li>
                        <li class=""><a data-test-id="home_header_compare_zenhaven_link" href="https://www.awarasleep.com/p/compare/zenhaven/">Vs Zenhaven</a></li>
                        <li class=""><a data-test-id="home_header_compare_brentwoodcedar_link" href="https://www.awarasleep.com/p/compare/brentwoodcedar/">Vs Brentwood</a></li>
                        <li class=""><a data-test-id="home_header_compare_birch_link" href="https://www.awarasleep.com/p/compare/birch/">Vs Birch Living</a></li>
                        <li class=""><a data-test-id="home_header_compare_happsy_link" href="https://www.awarasleep.com/p/compare/happsy/">Vs Happsy</a></li>
                        <li class=""><a data-test-id="home_header_compare_nest_link" href="https://www.awarasleep.com/p/compare/nest/">Vs Nest Bedding</a></li>
                        <li class=""><a data-test-id="home_header_compare_ghostbed_link" href="https://www.awarasleep.com/p/compare/ghostbed/">Vs Ghostbed</a></li>
                        <li class=""><a data-test-id="home_header_compare_loom-and-leaf_link" href="https://www.awarasleep.com/p/compare/loom-and-leaf/">Vs Loom &amp; Leaf</a></li>
                    </ul>
                </li>
                <li class="dropdown_qj6"><button type="button" class="dropdown__button_nqJ" data-test-id="home_header_environment"><span
                            class="dropdown__button-text_3Pu">Environment</span><svg xmlns="http://www.w3.org/2000/svg"
                            height="8" width="12" viewBox="0 0 12 8" fill="#4C3043" class="dropdown__icon_20r">
                            <path
                                d="M10.6 0.333282L6 4.93328L1.4 0.333282L0 1.73328L6 7.73328L12 1.73328L10.6 0.333282Z"
                                fill="#C56E43"></path>
                        </svg></button>
                    <ul class="dropdown__body_3O0">
                        <li class=""><a data-test-id="home_header_about_us_link" href="https://www.awarasleep.com/p/about/">About Us</a></li>
                        <li class=""><a data-test-id="home_header_certifications_link" href="https://www.awarasleep.com/p/certifications/">Certifications</a></li>
                        <li class=""><a data-test-id="home_header_sustainability_link" href="https://www.awarasleep.com/p/sustainability/">Sustainability</a></li>
                        <li class=""><a data-test-id="home_header_environment_all_natural_link" href="https://www.awarasleep.com/p/all-natural/">All Natural</a></li>
                        <li class=""><a data-test-id="home_header_non_toxic_link" href="https://www.awarasleep.com/p/non-toxic/">Non-Toxic</a></li>
                        <li class=""><a data-test-id="home_header_our_team_link" href="https://www.awarasleep.com/p/our-team/">Our Team</a></li>
                    </ul>
                </li>
                <li class=""><a data-test-id="home_header_reviews_link" href="/reviews">Reviews</a></li>
                <li class="dropdown_qj6"><button type="button" class="dropdown__button_nqJ" data-test-id="home_header_help"><span class="dropdown__button-text_3Pu">Help</span><svg
                            xmlns="http://www.w3.org/2000/svg" height="8" width="12" viewBox="0 0 12 8" fill="#4C3043"
                            class="dropdown__icon_20r">
                            <path
                                d="M10.6 0.333282L6 4.93328L1.4 0.333282L0 1.73328L6 7.73328L12 1.73328L10.6 0.333282Z"
                                fill="#C56E43"></path>
                        </svg></button>
                    <ul class="dropdown__body_3O0">
                        <li class=""><a data-test-id="home_header_faq_link" href="/faq">FAQs</a></li>
                        <li class=""><a data-test-id="home_header_help_trial_link" href="https://www.awarasleep.com/p/trial/">365 Trial</a></li>
                        <li class=""><a data-test-id="home_header_finance_link" href="https://www.awarasleep.com/p/finance-affirm/">Financing</a></li>
                    </ul>
                </li>
            </ul>