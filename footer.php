<footer data-test-id="footer" class="site-footer_1TJ">
            <section data-test-id="footer_certs_section" class="footer-certs_2Xv">
                <div class="footer-certs__h_2V1">Certifications</div>
                <div class="footer-certs__list_3NP">
                    <section class="footer-certs-list_18B">
                        <div class="footer-certs-list__item_1tb"><img class="item-image--opacity_1fq" lr="" lr-loader-triggers="screen" lr-loader-actions="image:https://media.awarasleep.com/awara/rainforest-logo-brown-v3.png?width=100p&amp;auto=webp" lr-revealer-triggers="screen" lr-revealer-actions="setSrc"
                                alt="Handmade" src="https://media.awarasleep.com/awara/rainforest-logo-brown-v3.png?width=100p&amp;auto=webp">
                        </div>
                        <div class="footer-certs-list__item_1tb"><img lr="" lr-loader-triggers="screen" lr-loader-actions="image:https://media.awarasleep.com/awara/certifications/100-natural-wool-3.png?width=100p&amp;auto=webp" lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="100% natural wool"
                                src="https://media.awarasleep.com/awara/certifications/100-natural-wool-3.png?width=100p&amp;auto=webp">
                        </div>
                        <div class="footer-certs-list__item_1tb"><img lr="" lr-loader-triggers="screen" lr-loader-actions="image:https://media.awarasleep.com/awara/certifications/no-poly-foams.svg" lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="No Poly Foams" src="https://media.awarasleep.com/awara/certifications/no-poly-foams.svg"></div>
                        <div class="footer-certs-list__item_1tb"><img lr="" lr-loader-triggers="screen" lr-loader-actions="image:https://media.awarasleep.com/awara/certifications/no-formaldegydus.svg" lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="No Formaldegydus" src="https://media.awarasleep.com/awara/certifications/no-formaldegydus.svg"></div>
                        <div class="footer-certs-list__item_1tb"><img lr="" lr-loader-triggers="screen" lr-loader-actions="image:https://media.awarasleep.com/awara/certifications/no-lead.svg" lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="No Lead" src="https://media.awarasleep.com/awara/certifications/no-lead.svg"></div>
                        <div class="footer-certs-list__item_1tb"><img lr="" lr-loader-triggers="screen" lr-loader-actions="image:https://media.awarasleep.com/awara/certifications/chemical-free.svg" lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Chemical Free" src="https://media.awarasleep.com/awara/certifications/chemical-free.svg"></div>
                        <div class="footer-certs-list__item_1tb"><img lr="" lr-loader-triggers="screen" lr-loader-actions="image:https://media.awarasleep.com/awara/certifications/water-based.svg" lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Water Based" src="https://media.awarasleep.com/awara/certifications/water-based.svg"></div>
                        <div class="footer-certs-list__item_1tb"><img lr="" lr-loader-triggers="screen" lr-loader-actions="image:https://media.awarasleep.com/awara/certifications/handmade.svg" lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Handmade" src="https://media.awarasleep.com/awara/certifications/handmade.svg"></div>
                    </section>
                </div>
            </section>
            <section data-test-id="footer_payment_section" class="footer-payments_3oy">
                <div class="footer-payments__col_2CR footer-payments__col--secure_12S">
                    <div class="footer-payments__h_2y5">Purchase Safely</div>
                    <div class="secures_1RQ">
                        <div class="secures__col_2YO">
                            <div class="secures__img_19k"><img lr="" lr-loader-triggers="screen" lr-loader-actions="image:https://media.awarasleep.com/awara/footer/secure-checkout.svg" lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Secure Checkout" src="https://media.awarasleep.com/awara/footer/secure-checkout.svg"></div>
                            <div>Secure Checkout</div>
                        </div>
                        <div class="secures__col_2YO">
                            <div class="secures__img_19k"><img lr="" lr-loader-triggers="screen" lr-loader-actions="image:https://media.awarasleep.com/awara/footer/encrypted-checkout.svg" lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Encrypted Checkout" src="https://media.awarasleep.com/awara/footer/encrypted-checkout.svg"></div>
                            <div>Encrypted Information</div>
                        </div>
                    </div>
                </div>
                <div class="footer-payments__col_2CR footer-payments__col--payments_3qU">
                    <div class="footer-payments__h_2y5 footer-payments__h--before-image_3au">Convenient Payment</div>
                    <div class="secures__img_19k"><img lr="" lr-loader-triggers="screen" lr-loader-actions="image:https://media.awarasleep.com/awara/footer/payments.png?width=100p&amp;auto=webp" lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Payments" src="https://media.awarasleep.com/awara/footer/payments.png?width=100p&amp;auto=webp"></div>
                </div>
                <div class="footer-payments__col_2CR footer-payments__col--delivery_3Og">
                    <div class="footer-payments__h_2y5 footer-payments__h--before-image_3au">Fast Delivery</div>
                    <div class="secures__img_19k"><img src="https://media.awarasleep.com/awara/footer/FedEx.png?width=100p&amp;auto=webp" alt="FedEx"></div>
                </div>
            </section>
            <?php get_sidebar('footer'); ?>
        </footer>
        
</body>

</html>