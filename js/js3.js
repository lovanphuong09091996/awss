(window["webpackJsonp"] = window["webpackJsonp"] || []).push([
    [103], {
        1369: function(e, t, r) { e.exports = { "cart-alert-box": "cart-alert-box_5T_", small: "small_1Jg", visible: "visible_2LO" } },
        328: function(e, t, r) {
            "use strict";
            r.r(t);
            (function(e, n) {
                r.d(t, "default", (function() { return E }));
                var l = r(21);
                var o = r.n(l);
                var a = r(518);
                var i = r.n(a);
                var c = r(1369);
                var u = r.n(c);

                function s(e) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { s = function e(t) { return typeof t } } else { s = function e(t) { return t && typeof Symbol === "function" && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t } } return s(e) }
                var f = { "./style.scss": { "cart-alert-box": "cart-alert-box_5T_", small: "small_1Jg", visible: "visible_2LO" } };

                function p(e, t) { if (!(e instanceof t)) { throw new TypeError("Cannot call a class as a function") } }

                function m(e, t) {
                    for (var r = 0; r < t.length; r++) {
                        var n = t[r];
                        n.enumerable = n.enumerable || false;
                        n.configurable = true;
                        if ("value" in n) n.writable = true;
                        Object.defineProperty(e, n.key, n)
                    }
                }

                function b(e, t, r) { if (t) m(e.prototype, t); if (r) m(e, r); return e }

                function v(e, t) { if (t && (s(t) === "object" || typeof t === "function")) { return t } return d(e) }

                function y(e) { y = Object.setPrototypeOf ? Object.getPrototypeOf : function e(t) { return t.__proto__ || Object.getPrototypeOf(t) }; return y(e) }

                function d(e) { if (e === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called") } return e }

                function h(e, t) {
                    if (typeof t !== "function" && t !== null) { throw new TypeError("Super expression must either be null or a function") }
                    e.prototype = Object.create(t && t.prototype, { constructor: { value: e, writable: true, configurable: true } });
                    if (t) _(e, t)
                }

                function _(e, t) { _ = Object.setPrototypeOf || function e(t, r) { t.__proto__ = r; return t }; return _(e, t) }
                var E = function(t) {
                    h(r, t);

                    function r(e) {
                        var t;
                        p(this, r);
                        t = v(this, y(r).call(this, e));
                        t.state = { products: null, visible: false };
                        t.onCartAlert = t.onCartAlert.bind(d(t));
                        t.cartAlertTimeout = null;
                        return t
                    }
                    b(r, [{ key: "componentDidMount", value: function e() { n.subscribe(this, "CartAlert") } }, { key: "componentWillUnmount", value: function e() { n.unsubscribe(this, "CartAlert") } }, {
                        key: "onCartAlert",
                        value: function e(t) {
                            var r = t.visible;
                            var n = t.products;
                            var l = this;
                            this.setState({ visible: r, products: n ? n : null });
                            if (!n && r) {
                                clearTimeout(this.cartAlertTimeout);
                                this.cartAlertTimeout = setTimeout((function() { l.setState({ visible: false, products: null }) }), 2e3)
                            }
                        }
                    }, {
                        key: "render",
                        value: function t() {
                            var r = this.state,
                                n = r.visible,
                                l = r.products;
                            return e.createElement("div", { id: "cart_alert", className: o()((n ? "cart-alert-box visible" : "cart-alert-box") + (!l ? " small" : ""), f) }, n ? l ? e.createElement("div", { id: "cart_alert_add" }, e.createElement("h5", null, "Adding to Cart ", e.createElement("div", { className: "pull-right" }, e.createElement(i.a, { type: "TailSpin", color: "#000000", height: 20, width: 20 }))), e.createElement("div", { className: "container" }, l.map((function(t, r) {
                                var n = t.properties,
                                    l = t.title;
                                return e.createElement("div", { key: "product_" + r, className: "row" }, e.createElement("div", { className: "col-xs-12" }, e.createElement("div", { className: "col-xs-10" }, e.createElement("p", null, l), n ? Array.isArray(n) ? n.map((function(t, r) { return e.createElement("p", { key: r }, t.name, ": ", t.value) })) : n : false), e.createElement("div", { className: "col-xs-2" }, e.createElement("p", null, e.createElement("b", null, t.price)))))
                            })))) : e.createElement("p", null, "Your cart is empty...") : false)
                        }
                    }]);
                    return r
                }(e.Component)
            }).call(this, r(2), r(13)["eventSystem"])
        }
    }
]);