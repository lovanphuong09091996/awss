(function(e) {
    function t(t) {
        var n = t[0];
        var r = t[1];
        var i, a, s = 0,
            c = [];
        for (; s < n.length; s++) {
            a = n[s];
            if (Object.prototype.hasOwnProperty.call(o, a) && o[a]) { c.push(o[a][0]) }
            o[a] = 0
        }
        for (i in r) { if (Object.prototype.hasOwnProperty.call(r, i)) { e[i] = r[i] } }
        if (u) u(t);
        while (c.length) { c.shift()() }
    }
    var n = {};
    var o = { 62: 0 };

    function r(e) { return i.p + "../public/javascripts/" + ({ 63: "polyfills", 64: "vendors~polyfills" }[e] || e) + "-awara." + { 63: "59c3fa67c8ec015c5f23", 64: "b43b7abec0a8640c793e" }[e] + ".js" }

    function i(t) {
        if (n[t]) { return n[t].exports }
        var o = n[t] = { i: t, l: false, exports: {} };
        e[t].call(o.exports, o, o.exports, i);
        o.l = true;
        return o.exports
    }
    i.e = function e(t) {
        var n = [];
        var a = o[t];
        if (a !== 0) {
            if (a) { n.push(a[2]) } else {
                var s = new Promise((function(e, n) { a = o[t] = [e, n] }));
                n.push(a[2] = s);
                var c = document.createElement("script");
                var u;
                c.charset = "utf-8";
                c.timeout = 120;
                if (i.nc) { c.setAttribute("nonce", i.nc) }
                c.src = r(t);
                var l = new Error;
                u = function(e) {
                    c.onerror = c.onload = null;
                    clearTimeout(d);
                    var n = o[t];
                    if (n !== 0) {
                        if (n) {
                            var r = e && (e.type === "load" ? "missing" : e.type);
                            var i = e && e.target && e.target.src;
                            l.message = "Loading chunk " + t + " failed.\n(" + r + ": " + i + ")";
                            l.name = "ChunkLoadError";
                            l.type = r;
                            l.request = i;
                            n[1](l)
                        }
                        o[t] = undefined
                    }
                };
                var d = setTimeout((function() { u({ type: "timeout", target: c }) }), 12e4);
                c.onerror = c.onload = u;
                document.head.appendChild(c)
            }
        }
        return Promise.all(n)
    };
    i.m = e;
    i.c = n;
    i.d = function(e, t, n) { if (!i.o(e, t)) { Object.defineProperty(e, t, { enumerable: true, get: n }) } };
    i.r = function(e) {
        if (typeof Symbol !== "undefined" && Symbol.toStringTag) { Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }) }
        Object.defineProperty(e, "__esModule", { value: true })
    };
    i.t = function(e, t) {
        if (t & 1) e = i(e);
        if (t & 8) return e;
        if (t & 4 && typeof e === "object" && e && e.__esModule) return e;
        var n = Object.create(null);
        i.r(n);
        Object.defineProperty(n, "default", { enumerable: true, value: e });
        if (t & 2 && typeof e != "string")
            for (var o in e) i.d(n, o, function(t) { return e[t] }.bind(null, o));
        return n
    };
    i.n = function(e) {
        var t = e && e.__esModule ? function t() { return e["default"] } : function t() { return e };
        i.d(t, "a", t);
        return t
    };
    i.o = function(e, t) { return Object.prototype.hasOwnProperty.call(e, t) };
    i.p = "/assets/";
    i.oe = function(e) { console.error(e); throw e };
    var a = window["webpackJsonp"] = window["webpackJsonp"] || [];
    var s = a.push.bind(a);
    a.push = t;
    a = a.slice();
    for (var c = 0; c < a.length; c++) t(a[c]);
    var u = s;
    return i(i.s = 303)
})({
    303: function(e, t, n) {
        window.apiAuthToken = null;
        window.reloadApp = function() { if ("dispatchEvent" in window && "CustomEvent" in window) { window.dispatchEvent(new CustomEvent("Reload")) } };
        var o = function e(t) { return !navigator || !navigator.userAgent || t.test(navigator.userAgent) };
        var r = { isBot: o(/prerender|bot|console|structured|apis-google/i), oldBrowsers: o(/iPhone OS 8|msie|trident/i), isInsightsTool: o(/lighthouse|insights/i) };
        var i = function e() {
            try {
                var t = window.QueryStringToJSON(window.location.search);
                if (t && t["cpn"]) {
                    var n = new Date;
                    n.setTime((new Date).getTime() + 18e5);
                    docCookies.setItem("active_cpn", t["cpn"].toLowerCase(), n.toGMTString(), "/", null, !window.Configuration.ENV)
                }
                if (t && t.eid) {
                    var o = t.eid.split(",");
                    o.forEach((function(e) { var t = e.split("-"); if (t.length > 1) { window[t[0]] = t[1] === "true" || t[1] === "false" ? t[1] === "true" : t[1] } }))
                }
            } catch (e) { console.error(e) }
        };
        var a = function e(t, n) {
            var o = new XMLHttpRequest;
            var r = "https://api.residenthome.com/featureflags/operational?brand=" + n + "&env=" + t;
            o.onreadystatechange = function() {
                if (this.readyState === 4 && this.status === 200) {
                    try {
                        var e = JSON.parse(o.responseText);
                        if (e.status === "ok") {
                            window.featureFlags = e.result;
                            window.reloadApp()
                        }
                    } catch (e) { console.error(e) }
                }
            };
            o.open("GET", r, true);
            o.send()
        };
        var s = function e() {
            if (true) {
                (function(e, t, n, o, r) {
                    e[o] = e[o] || [];
                    e[o].push({ "gtm.start": (new Date).getTime(), event: "gtm.js" });
                    var i = t.getElementsByTagName(n)[0],
                        a = t.createElement(n),
                        s = o != "dataLayer" ? "&l=" + o : "";
                    a.async = true;
                    a.src = "https://www.googletagmanager.com/gtm.js?id=" + r + s;
                    i.parentNode.insertBefore(a, i)
                })(window, document, "script", "dataLayer", "GTM-WCFDZSQ")
            }
        };
        var c = function e() {
            if (true) {
                (function(e, t, n, o, r, i, a) {
                    e["GoogleAnalyticsObject"] = r;
                    e[r] = e[r] || function() {
                        (e[r].q = e[r].q || []).push(arguments)
                    }, e[r].l = 1 * new Date;
                    i = t.createElement(n), a = t.getElementsByTagName(n)[0];
                    i.async = 1;
                    i.src = o;
                    a.parentNode.insertBefore(i, a)
                })(window, document, "script", "https://www.google-analytics.com/analytics.js", "ga");
                ga("create", "UA-83380638-20", "auto");
                true && ga("require", "GTM-N2LBNC8")
            }
        };
        var u = function e(t, n, o, r, i) {
            var a = document.createElement(o);
            if (r) { a.setAttribute("rel", r) }
            a.setAttribute("type", n);
            a.onload = function() { i && i() };
            a.setAttribute(t.type, t.value);
            document.getElementsByTagName("head")[0].appendChild(a)
        };
        var l = function e(t) {
            if (window.Promise) { t && t(); return }
            u({ type: "src", value: "/assets/javascripts/promise.js" }, "text/javascript", "script", null, (function() {
                var e = setInterval((function() {
                    if (window.Promise) {
                        clearInterval(e);
                        t && t()
                    }
                }), 25)
            }))
        };
        var d = function t() {
            var n = window.Configuration.ENV || "production";
            try {
                window.docCookies = {
                    getItem: function e(t) { return t ? decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(t).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null : null },
                    setItem: function e(t, n, o, r, i, a) {
                        if (!t || /^(?:expires|max\-age|path|domain|secure)$/i.test(t)) return !1;
                        var s = "";
                        if (o) switch (o.constructor) {
                            case Number:
                                s = o === 1 / 0 ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + o;
                                break;
                            case String:
                                s = "; expires=" + o;
                                break;
                            case Date:
                                s = "; expires=" + o.toUTCString()
                        }
                        return document.cookie = encodeURIComponent(t) + "=" + encodeURIComponent(n) + s + (i ? "; domain=" + i : "") + (r ? "; path=" + r : "") + (a ? "; secure" : ""), !0
                    },
                    removeItem: function e(t, n, o) { return this.hasItem(t) ? (document.cookie = encodeURIComponent(t) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (o ? "; domain=" + o : "") + (n ? "; path=" + n : ""), !0) : !1 },
                    hasItem: function e(t) { return !t || /^(?:expires|max\-age|path|domain|secure)$/i.test(t) ? !1 : new RegExp("(?:^|;\\s*)" + encodeURIComponent(t).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=").test(document.cookie) },
                    keys: function e() { for (var t = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/), n = t.length, o = 0; n > o; o++) { t[o] = decodeURIComponent(t[o]) } return t }
                };
                true && "undefined" != typeof e.exports && (e.exports = docCookies);
                try {
                    window.apiAuthToken = docCookies.getItem("authToken");
                    window.geoloc = JSON.parse(docCookies.getItem("geoloc"));
                    window.Configuration.overrideAPIURL = docCookies.getItem("overrideAPIURL")
                } catch (e) { console.error(e) }
                window.featureFlags = {};
                window.isInsightsTool = r.isInsightsTool;
                window.enableScripts = n !== "test" && !r.isBot && !r.isInsightsTool;
                window.globalTimeoutValue = window.apiAuthToken && !r.isInsightsTool ? 0 : 5e3;
                window.QueryStringToJSON = function(e) {
                    var t = {};
                    try {
                        if (e) {
                            var n = e.slice(1).split("&");
                            n.forEach((function(e) {
                                e = e.split("=");
                                t[e[0]] = decodeURIComponent(e[1] || "")
                            }))
                        }
                    } catch (e) {}
                    return JSON.parse(JSON.stringify(t))
                }
            } catch (e) { console.error(e) }
            i();
            if (window.enableScripts) {
                setTimeout((function() { window.Configuration.APP_FONTS && u({ type: "href", value: window.Configuration.APP_FONTS }, "text/css", "link", "stylesheet") }), window.globalTimeoutValue);
                setTimeout((function() { n === "production" && s() }), window.location.pathname.includes("checkout") ? 0 : 5e3);
                a(n, "awara")
            }!window.isInsightsTool && n === "production" && c();
            window.Configuration.APP_BUNDLE && u({ type: "src", value: window.Configuration.APP_BUNDLE }, "text/javascript", "script", null);
            window.Configuration.APP_CSS && u({ type: "href", value: window.Configuration.APP_CSS }, "text/css", "link", "stylesheet")
        };
        l((function() { if ((r.isBot || r.oldBrowsers) && !r.isInsightsTool) { Promise.all([n.e(64), n.e(63)]).then(n.bind(null, 323)).then((function() { d() })) } else { d() } }))
    }
});