<?php
   
/*================================================================================
    *3. Khai bao he thong widget cua themes
    =================================================================================*/
    function aws_widgets_init() {

      register_sidebar( 
        array(
            'name'          => __( 'footer1', 'AWS' ),
            'id'            => 'footer1',
            'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'AWS' ),
            'before_widget' => '<ul id="%1$s" class="menu_3Nt %2$s">',
            'after_widget'  => '</ul>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        ) 
      );

    register_sidebar( 
      array(
        'name'          => __( 'footer2', 'AWS' ),
        'id'            => 'footer2',
        'description'   => __( 'Add widgets here to appear in your footer.', 'AWS' ),
        'before_widget' => '<ul id="%1$s" class="menu_3Nt menu--information_13a %2$s">',
        'after_widget'  => '</ul>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) 
  );
  
    register_sidebar( 
      array(
        'name'          => __( 'footer3', 'AWS' ),
        'id'            => 'footer3',
        'description'   => __( 'Add widgets here to appear in your footer.', 'AWS' ),
        'before_widget' => '<div id="%1$s" class="footer__operational-row_1qh %2$s">',
        'after_widget'  => '</div>',  
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) 
  );
}
add_action( 'widget_init', 'aws_widget_init' );

    /*================================================================================
    *1. Nap nhung tap tin css vao theme
    =================================================================================*/

    function aws_styles() {
        wp_register_style( 'style1', get_template_directory_uri() . '/css/style1.min.css', 'all' );
        wp_enqueue_style( 'style1' );

        wp_register_style( 'style2', get_template_directory_uri() . '/css/style2.min.css', 'all' );
        wp_enqueue_style( 'style2' );

        wp_register_style( 'style3', get_template_directory_uri() . '/css/style3.min.css', 'all' );
        wp_enqueue_style( 'style3' );

        wp_register_style( 'style4', get_template_directory_uri() . '/css/style4.min.css', 'all' );
        wp_enqueue_style( 'style4' );

        wp_register_style( 'style5', get_template_directory_uri() . '/css/style5.min.css', 'all' );
        wp_enqueue_style( 'style5' );
      }
      add_action( 'wp_enqueue_scripts', 'aws_styles' );

      /*================================================================================
    *2. Nap nhung tap tin js vao theme
    =================================================================================*/
 ?>