<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name ="viewport" content = "width = device-width, initial-scale=1">
    <title>
    <?php
        wp_title('|', true, 'right');
        bloginfo('name');
    ?>
    </title>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url') ?>" />
    <?php wp_head(); ?>
</head>

<body data-wz_page_group="home" sr-scroll-left="0" sr-scroll-top="2600">
    <div id="app">
        <div id="cart_alert" class="cart-alert-box_5T_ small_1Jg"></div>
        <div name="app-header"></div>
        <div class="header__placeholder_8pH"></div>
        <div class="header__backdrop_4pF"></div>
        <header id="site-header" class="site-header_1WD"><a class="logo_19w" data-test-id="logo" href="/"><svg
                    viewBox="0 0 150 26" fill="#4C3043" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M15.5727 17.4732H4.47176L1.49059 25.3731H0L9.57113 0H13.4545L23.4571 25.3731H18.6323L15.5727 17.4732ZM9.84571 2.91869L4.90324 16.2668H15.102L9.84571 2.91869Z">
                    </path>
                    <path
                        d="M43.8545 4.47532L37.8137 25.3731H34.1657L25.9675 0H30.6354L37.5784 21.4815L43.7761 0H47.071L53.9748 21.4815L60.2509 0H61.6631L54.2494 25.3731H50.6014L43.8545 4.47532Z">
                    </path>
                    <path
                        d="M80.5308 17.4732H69.4298L66.4879 25.3731H64.9973L74.5292 0H78.4126L88.4152 25.3731H83.5904L80.5308 17.4732ZM74.8038 2.91869L69.8221 16.2668H80.0208L74.8038 2.91869Z">
                    </path>
                    <path
                        d="M102.223 13.5427V25.3731H97.712V0H105.792C113.089 0 116.854 2.25712 116.854 6.69352C116.854 10.3516 113.638 12.4142 109.323 12.8422V12.9201C112.775 13.2703 115.207 14.6713 116.109 18.4461L117.756 25.3342H113.128L111.441 18.0569C110.578 14.321 108.852 13.5427 105.753 13.5427H102.223ZM102.223 12.3752H105.714C110.225 12.3752 112.147 10.0792 112.147 6.77136C112.147 3.38568 110.186 1.24531 105.753 1.24531H102.223V12.3752Z">
                    </path>
                    <path
                        d="M142.116 17.4732H131.015L128.073 25.3731H126.582L136.114 0H139.998L150 25.3731H145.175L142.116 17.4732ZM136.428 2.91869L131.446 16.2668H141.645L136.428 2.91869Z">
                    </path>
                </svg></a>
            <?php get_sidebar('menu'); ?>
            <div class="actions_gY2"><a class="tablet-only_2pL" href="/mattress">Shop Mattress</a>
                <div id="header_chat" class="chat_29l"><svg xmlns="http://www.w3.org/2000/svg" height="40px" width="40px" viewBox="0 0 50 50" fill="#4C3043" class="chat__icon_2rD icon_3C6">
                        <g>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M10.8893 37.5H47.3684V2.63158H2.63158V44.3815L10.8893 37.5ZM0 50V0H50V40.1316H11.8421L0 50Z">
                            </path>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M18.4211 15.7895L18.4211 23.6842H15.7895L15.7895 15.7895H18.4211Z"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M26.3158 15.7895L26.3158 23.6842H23.6842L23.6842 15.7895H26.3158Z"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M34.2105 15.7895L34.2105 23.6842H31.5789L31.5789 15.7895H34.2105Z"></path>
                        </g>
                    </svg></div>
                <div data-test-id="cart" class="cart_S6b">
                    <div></div>
                </div>
                <div data-test-id="nav_toggle_burger" class="nav-toggle_2jK"><svg xmlns="http://www.w3.org/2000/svg" height="13px" width="18px" viewBox="0 0 18 13" fill="none" class="icon_3C6">
                        <g>
                            <line y1="0.5" x2="18" y2="0.5" stroke="#4C3043"></line>
                            <line y1="4.5" x2="18" y2="4.5" stroke="#4C3043"></line>
                            <line y1="8.5" x2="18" y2="8.5" stroke="#4C3043"></line>
                            <line y1="12.5" x2="18" y2="12.5" stroke="#4C3043"></line>
                        </g>
                    </svg></div>
            </div>
        </header>